import os


def get_base_path(file):
    base_path = os.path.dirname(os.path.abspath(file)) + '/'
    return base_path


base_path = get_base_path(__file__)

