from .automata import GameOfLife
from .utils import base_path

loaf_initial = "      \n" \
               "  **  \n" \
               " *  * \n" \
               "  * * \n" \
               "   *  \n" \
               "      "
loaf_sample = GameOfLife(6, 6, loaf_initial)
loaf_sample.render(10)
loaf_sample.animate(name=base_path + 'loaf')


beacon_initial = "      \n" \
                 " **   \n" \
                 " *    \n" \
                 "    * \n" \
                 "   ** \n" \
                 "      "
beacon_sample = GameOfLife(6, 6, beacon_initial)
beacon_sample.render(10)
beacon_sample.animate(name=base_path + 'beacon')


glider_initial = "      \n" \
                 "      \n" \
                 "   *  \n" \
                 " * *  \n" \
                 "  **  \n" \
                 "      "
glider_sample = GameOfLife(6, 6, glider_initial)
glider_sample.render(10)
glider_sample.animate(name=base_path + 'glider')


eater_and_glider_initial = "          \n" \
                           "      *   \n" \
                           "     *    \n" \
                           "     ***  \n" \
                           "          \n" \
                           "   **     \n" \
                           "    *     \n" \
                           " ***      \n" \
                           " *        \n" \
                           "          "
eater_and_glider_sample = GameOfLife(10, 10, eater_and_glider_initial)
eater_and_glider_sample.render(10)
eater_and_glider_sample.animate(name=base_path + 'eater_and_glider')
