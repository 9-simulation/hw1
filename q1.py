from .automata import CA1D
from .utils import base_path

sample = CA1D(201, [101])

# Rule = 90
sample.render(90, 200)
sample.show(base_path + 'q1-90')
